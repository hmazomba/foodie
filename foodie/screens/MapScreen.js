import React, { Component } from 'react'
import { Text, StyleSheet, View, Button} from 'react-native'
import FetchLocation from '../components/FetchLocation';
import UsersMap from '../components/UsersMap';
import {Constants, Location, Permissions} from 'expo';
export default class MapScreen extends Component {
  state = {
    userLocation: null, 
    errorMessage: null
  }
  /* getUserLocationHandler = () => {
    
    /* navigator.geolocation.getCurrentPosition(position => {
        this.setState({
          userLocation: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            longitudeDelta: 0.05,
            latitudeDelta: 0.04
          }
        })
    },err => console.log(err))
  } */
  componentWillMount(){
    this.getLocationAsync();
  } 
  
  getLocationAsync = async (props) => {
    let {status} = await Permissions.askAsync(Permissions.LOCATION);
    if(status !== 'granted'){
      this.setState({
        errorMessage: 'Permission to access location was ended',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    await this.setState({
      userLocation: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
    }
  });
  }
  render() {
    return (
      <View>
        <UsersMap userLocation={this.state.userLocation}/>
        <FetchLocation onGetLocation={this.getLocationAsync}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
      },
})
