import React, { Component } from 'react'
import { Text, StyleSheet, View, Button, Image } from 'react-native'
import { Container, Content, Item, Input, Button } from 'native-base';
import Logo from '../assets/img/food-on-a-tray.jpg';

export default class LoginScreen extends Component {
    static navigationOptions ={
        header : null
    }
  render() {
    return (
      <Container style={styles.container}>
      <Content>
        <View style={styles.topContainer}>
          <Image source={Logo} style={styles.logo} />
          <Text style={styles.h1}>Welcome To Foodie</Text>
        </View>
        <View>
          <Item>
            <Input placeholder="Full Name"/>
          </Item>
          <Item>
            <Input placeholder="Email"/>
          </Item>
          <Item>
            <Input placeholder="Password" secureTextEntry={true}/>
          </Item>
        </View>
        <View style={styles.bottomContainer}>
          <Button>
            <Text onPress={()=> this.props.navigation.navigate('Map')}>Sign Up</Text>
          </Button>
        </View>
      </Content>        
      </Container>
    )
  }
}

const styles = StyleSheet.create({
    h1:{
      fontSize: 30,
      fontWeight: 'bold',
      color: 'white',
    },

    topContainer: {
      flex: 2, 
      justifyContent: 'center',
      alignItems: 'center',
    }, 
    middleContainer: {
      flex: 3, 
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    bottomContainer: {
      justifyContent: 'flex-end',
      width: '90%',
      margin: 20,
      padding: 20,
    },
    logo: {
      width: 150,
      height:100,
      padding: 10,
    }, 
    container: {
      backgroundColor: 'grey',
      flex: 5,
      alignItems: 'center',
      zIndex: 0,

    }
})
