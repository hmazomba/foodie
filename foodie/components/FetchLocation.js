import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

export default class FetchLocation extends Component {
  render() {
    return (
      <Button title="Get Location" onPress={this.props.onGetLocation}/>
    );
  }
}
