import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createStackNavigator} from 'react-navigation'; 
import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import MapScreen from './screens/MapScreen';
export default class App extends React.Component {
  render() {
    return (
      <Navigator/>
    );
  }
}
const Navigator = createStackNavigator ({
  Login: LoginScreen,
  Home: HomeScreen, 
  Map: MapScreen
})
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
